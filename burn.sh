#!/bin/bash

# These functions are utilities for format and burn new copies for new Pimballs

# This function format a new sdcard on fat32 for burn
# This function receive the letter of device name
format () {
    chk_args $1
    for unity_name in $@
    do
        echo "Creating partition table on /dev/sd$unity_name"
        umount /dev/sd$unity_name
        dd if=/dev/zero of=/dev/sd$unity_name bs=512 count=1 conv=notrunc
        echo "Format to FAT32 /dev/sd$unity_name"
        mkfs.vfat "/dev/sd$unity_name"
    done
}

get_image() {
    chk_args $1
    chk_args $2
    #sudo dcfldd if=raspbian-7mar18.img | pee "sudo dcfldd of=/dev/sd"{b..i}
    echo "Get Image from $1 to $2"
    sleep 5
    date
    dcfldd if=$1 of=$2
    date
}

burn_sd() {
    chk_args $1
    chk_args $2
    echo "Burn image $1 to /dev/sd$2"
    sleep 5
    date
    sudo dcfldd if=$1 | pee "sudo dcfldd of=/dev/sd"$2
    date
}

chk_args () {
    if [ -z "$1" ]; then
        echo -e "\e[1m\e[91m The name of device is required.\e[0m"
        exit
    fi
}


$@
